const express = require('express');
const router = express.Router();
const Todo = require('../models/todo');

router.post('/', (req, res) => {
  const todo = new Todo({
    note: req.body.note,
    done: req.body.done,
  });

  todo.save((err) => {
    if (err) {
        console.log({ err });
        return res.status(500).send('ERROR!');
    }
    res.send('Todo Created successfully');
  });
});

router.get('/', (req, res) => {
  Todo.find((err, todos) => {
    if (err) return res.status(500).send('ERROR!');
    res.send(todos);
  });
});

router.get('/:id', (req, res) => {
  Todo.findById(req.params.id, (err, todo) => {
    if (err) return res.status(500).json(err);
    if (!todo) return res.status(404).send("not found!");
    res.send(todo);
  });
});

router.put('/:id', (req, res) => {
  Todo.findByIdAndUpdate(req.params.id, {$set: req.body}, (err, todo) => {
    if (err) return res.status(500).json(err);
    res.send('todo udpated.');
  });
});

router.delete('/:id', (req, res) => {
  Todo.findByIdAndRemove(req.params.id, (err) => {
    if (err) return res.status(500).json(err);
    res.send('Deleted successfully!');
  });
});

module.exports = router
