const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
    note: {type: String, required: true, max: 20},
    done: {type: Boolean, required: false},
});

module.exports = mongoose.model('Todo', TodoSchema);
