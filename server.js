const mongoose = require('mongoose');
const express = require('express')
const app = express()
const port = 4000
const bodyParser = require('body-parser');

// routes:
const todos = require('./routes/todos');

// mongoose setup:
const dev_db_url = 'mongodb://localhost:27017';
const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// body-parser:
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use('/todos', todos);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
